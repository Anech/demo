# This is a sample Python script.
import numpy as np
import tensorflow as tf


def print_hi(name):
    # This function prints "Hello <name>!"
    print(f'Hello {name}!')


if __name__ == '__main__':
    print_hi('world')
    print(f'This TensorFlow library is version {tf.__version__}')
    print(f'This NumPy library is version {np.__version__}')


